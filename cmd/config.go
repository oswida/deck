package cmd

import (
	"deck/util"
	"errors"
	"fmt"

	"github.com/desertbit/grumble"
	"github.com/k0kubun/pp"
	"github.com/logrusorgru/aurora"
)

func init() {
	cfg := &grumble.Command{
		Name:      "config",
		Help:      "configuration mode",
		Aliases:   []string{"cf"},
		Usage:     "config",
		AllowArgs: false,
	}
	// Workspaces
	App.AddCommand(cfg)
	ws := &grumble.Command{
		Name:      "workspace",
		Help:      "configure workspaces",
		Aliases:   []string{"ws"},
		Usage:     "workspace [add]",
		AllowArgs: false,
		Run:       listWorkspaces,
	}
	cfg.AddCommand(ws)
	wsadd := &grumble.Command{
		Name:      "add",
		Help:      "add workspace",
		Aliases:   []string{},
		Usage:     "add <name>",
		AllowArgs: true,
		Run:       addWorkspace,
		Completer: util.DirComplete,
	}
	ws.AddCommand(wsadd)

	// Files for Todos
	tds := &grumble.Command{
		Name:      "todo",
		Help:      "configure todo files",
		Aliases:   []string{"td"},
		Usage:     "todo [add]",
		AllowArgs: false,
		Run:       listTodos,
	}
	cfg.AddCommand(tds)
	tdadd := &grumble.Command{
		Name:      "add",
		Help:      "add todo file",
		Aliases:   []string{},
		Usage:     "add <name>",
		AllowArgs: true,
		Completer: util.FileComplete,
		Run:       addTodo,
	}
	tds.AddCommand(tdadd)
}

func listWorkspaces(c *grumble.Context) error {
	var ws []util.DbWorkspace
	err := util.Db.All(&ws)
	if err != nil {
		return err
	}
	wslist := []string{}
	for _, w := range ws {
		wslist = append(wslist, w.Name)
	}
	if len(wslist) > 0 {
		ws := util.AskSelectWithCancel("Workspace: ", wslist)
		ww := util.FindWorkspaceByName(ws)
		if ww != nil {
			action := util.AskSelectWithCancel("Action: ", []string{"edit", "delete"})
			switch action {
			case "delete":
				pp.Println(ww)
				if util.AskConfirm("Delete?") {
					err := util.Db.DeleteStruct(ww)
					if err != nil {
						return err
					} else {
						util.PrintSuccess(ws, " removed from workspace list")
					}
				}
			case "edit":
				nm := util.AskString("Name: ", ww.Name)
				if nm != "" {
					ww.Name = nm
				}
				pth := util.AskString("Path: ", ww.Path)
				if pth != "" {
					ww.Path = pth
				}
				pp.Println(ww)
				if util.AskConfirm("Save?") {
					err = util.Db.Save(ww)
					if err != nil {
						return err
					}
				}
			}
		}
	} else {
		util.PrintInfo("workspace list is empty")
	}
	return nil
}

func addWorkspace(c *grumble.Context) error {
	if len(c.Args) <= 0 {
		return errors.New("Missing workspace name")
	}
	path := util.ExpandPath(c.Args[0], true)
	if util.FileExists(path) {
		name := util.AskString("Workspace name: ", "")
		if name != "" {
			tosave := util.DbWorkspace{
				Name: name,
				Path: path,
			}
			pp.Println(tosave)
			if util.AskConfirm("Save?") {
				err := util.Db.Save(&tosave)
				if err != nil {
					return err
				} else {
					fmt.Println(aurora.Green("success: "), path, " added to workspace list")
				}
			}
		}
	} else {
		return fmt.Errorf("%v does not exist", path)
	}
	return nil
}

func listTodos(c *grumble.Context) error {
	var todos []util.DbTodo
	err := util.Db.All(&todos)
	if err != nil {
		return err
	}
	list := []string{}
	for _, td := range todos {
		list = append(list, td.Name)
	}
	if len(list) > 0 {
		tfile := util.AskSelectWithCancel("Todo file: ", list)
		if tfile != "" {
			t := util.FindTodoByName(tfile)
			if t != nil {
				action := util.AskSelectWithCancel("Action: ", []string{"edit", "delete"})
				switch action {
				case "delete":
					pp.Println(t)
					crm := util.AskConfirm("Delete?")
					if crm {
						err := util.Db.DeleteStruct(t)
						if err != nil {
							return err
						}
					}
				case "edit":
					nm := util.AskString("Name: ", t.Name)
					if nm != "" {
						t.Name = nm
					}
					pth := util.AskString("Path: ", t.Path)
					if pth != "" {
						t.Path = pth
					}
					pp.Println(t)
					if util.AskConfirm("Save?") {
						err = util.Db.Save(t)
						if err != nil {
							return err
						}
					}
				}
			}
		}
	} else {
		util.PrintInfo("list of todo files is empty")
	}
	return nil
}

func addTodo(c *grumble.Context) error {
	if len(c.Args) <= 0 {
		return fmt.Errorf("Missing todo file path")
	}
	path := util.ExpandPath(c.Args[0], true)
	if util.FileExists(path) {
		tname := util.AskString("Todo name: ", "")
		if tname != "" {
			tosave := util.DbTodo{
				Name: tname,
				Path: path,
			}
			pp.Print(tosave)
			save := util.AskConfirm("Save?")
			if save {
				err := util.Db.Save(&tosave)
				if err != nil {
					util.PrintError(err)
				}
			}
		}
	} else {
		return fmt.Errorf("%v does not exist", path)
	}
	return nil
}
