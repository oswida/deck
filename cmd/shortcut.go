package cmd

import (
	"deck/util"

	"github.com/desertbit/grumble"
)

func AddStandardAliases(app *grumble.App) {
	ex, _, _ := app.Commands().FindCommand([]string{"exit"})
	if ex != nil {
		ex.Aliases = []string{"ex", "quit", "q"}
	} else {
		util.PrintError("Cannot find command exit")
	}
	ex, _, _ = app.Commands().FindCommand([]string{"help"})
	if ex != nil {
		ex.Aliases = []string{"h"}
	} else {
		util.PrintError("Cannot find command help")
	}
}
