package cmd

import (
	"deck/util"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"sort"
	"strings"

	"github.com/desertbit/grumble"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"
	"github.com/jedib0t/go-pretty/table"
	a "github.com/logrusorgru/aurora"
)

func init() {
	ml := &grumble.Command{
		Name:      "mail",
		Help:      "e-mail support",
		Aliases:   []string{"ml"},
		Usage:     "mail <credential name flag>",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "mail credential name (required)")
			f.Int("x", "maxnum", 10, "maximum number of messages shown at once")
			f.String("f", "filter", "", "filter mail entries - subject should contain string")
			f.Bool("a", "all", false, "show all messages, not only unread")
			f.Bool("i", "interactive", false, "interactive mode")
		},
		Run: mailRun,
	}
	App.AddCommand(ml)
	ms := &grumble.Command{
		Name:      "show",
		Help:      "show mail body",
		Aliases:   []string{"sh"},
		Usage:     "show <credential name flag> <message sequence number flag>",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "mail credential name (required)")
			f.Int("m", "message", -1, "message sequence number (required)")
		},
		Run: mailShowRun,
	}
	ml.AddCommand(ms)
	md := &grumble.Command{
		Name:      "delete",
		Help:      "delete mail message",
		Aliases:   []string{"del"},
		Usage:     "delete <credential name flag> <message sequence number flag>",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "mail credential name (required)")
			f.Int("m", "message", -1, "message sequence number (required)")
			f.Bool("p", "purge", false, "purge mailbox after delete")
		},
		Completer: util.MailComplete,
		Run:       mailDeleteRun,
	}
	ml.AddCommand(md)
}

func mailRun(c *grumble.Context) error {
	nm := c.Flags.String("name")
	maxnum := c.Flags.Int("maxnum")
	if nm != "" {
		cret, err := util.DecryptCredentialData(App, nm)
		if err == nil {
			filter := c.Flags.String("filter")
			ia := c.Flags.Bool("interactive")
			return imapInboxAction(cret, func(mbox *imap.MailboxStatus, cl *client.Client) error {
				if c.Flags.Bool("all") {
					return mailGetMessages(nm, mbox, cl, maxnum, filter, ia, false)
				} else {
					return mailGetMessages(nm, mbox, cl, maxnum, filter, ia, true)
				}
			})
		} else {
			return err
		}
	} else {
		return mailListRun(c)
	}
}

func mailShowRun(c *grumble.Context) error {
	nm := c.Flags.String("name")
	msgid := c.Flags.Int("message")
	if nm != "" && msgid >= 0 {
		cret, err := util.DecryptCredentialData(App, nm)
		if err == nil {
			return imapInboxAction(cret, func(mbox *imap.MailboxStatus, cl *client.Client) error {
				criteria := imap.NewSearchCriteria()
				criteria.SeqNum = new(imap.SeqSet)
				criteria.SeqNum.AddNum(uint32(msgid))
				uids, err := cl.Search(criteria)
				if err != nil {
					return err
				}
				seqset := new(imap.SeqSet)
				seqset.AddNum(uids...)
				return mailGetMessageBody(cl, seqset)
			})
		} else {
			return err
		}
	} else {
		return fmt.Errorf("Credential name and message number required")
	}
}

func mailListRun(c *grumble.Context) error {
	creds := util.ListCredentialByType("mail")
	fmt.Println()
	rows := []table.Row{}
	for _, cc := range *creds {
		cret, err := util.DecryptCredentialData(App, cc.Name)
		if err == nil {
			err = imapInboxAction(cret, func(mbox *imap.MailboxStatus, cl *client.Client) error {
				unseen, total := mailGetCounts(mbox, cl)
				rows = append(rows, table.Row{
					a.BrightGreen(cc.Name),
					a.Cyan(total),
					a.Cyan(unseen),
				})
				return nil
			})
			if err != nil {
				return err
			}
		}
	}
	util.PrintTable("Mailbox summary", table.Row{"Name", "Total", "Unread"}, rows, true)
	fmt.Println()
	return nil
}

func mailDeleteRun(c *grumble.Context) error {
	nm := c.Flags.String("name")
	msgid := c.Flags.Int("message")
	if nm != "" && msgid >= 0 {
		cret, err := util.DecryptCredentialData(App, nm)
		if err == nil {
			return imapInboxAction(cret, func(mbox *imap.MailboxStatus, cl *client.Client) error {
				criteria := imap.NewSearchCriteria()
				criteria.SeqNum = new(imap.SeqSet)
				criteria.SeqNum.AddNum(uint32(msgid))
				uids, err := cl.Search(criteria)
				if err != nil {
					return err
				}
				seqset := new(imap.SeqSet)
				seqset.AddNum(uids...)
				return mailDeleteMessage(cl, seqset, c.Flags.Bool("purge"))
			})
		} else {
			return err
		}
	} else {
		return fmt.Errorf("Credential name and message number required")
	}
}

func imapInboxAction(
	cred *util.DbCredential,
	fn func(mbox *imap.MailboxStatus, cl *client.Client) error,
) error {
	if cred == nil {
		return fmt.Errorf("Empty credential")
	}
	cl, err := client.DialTLS(cred.Location, nil)
	if err != nil {
		return err
	}
	defer cl.Logout()
	if err := cl.Login(cred.Ident, cred.Secret); err != nil {
		return err
	}
	mbox, err := cl.Select("INBOX", false)
	if err != nil {
		return err
	}
	return fn(mbox, cl)
}

func formatMessageLine(msg *imap.Message) string {
	fromStr := []string{}
	for _, ad := range msg.Envelope.From {
		fromStr = append(fromStr, ad.PersonalName)
	}
	dat := msg.Envelope.Date.Format("2006-01-02")
	num := a.Magenta(msg.SeqNum)
	sign := a.Magenta("✉ ")
	if util.ContainsString(msg.Flags, imap.SeenFlag) {
		sign = a.Green("✉ ")
		num = a.Green(msg.SeqNum)
	}
	return fmt.Sprintf("%v %v %v %s %v",
		sign,
		num,
		a.Yellow(dat),
		msg.Envelope.Subject,
		a.Blue(strings.Join(fromStr, ",")),
	)
}

func mailGetMessages(
	credname string,
	mbox *imap.MailboxStatus,
	cl *client.Client,
	num int,
	filter string,
	ia bool,
	unread bool) error {
	criteria := imap.NewSearchCriteria()
	criteria.WithoutFlags = []string{imap.DeletedFlag}
	if unread {
		criteria.WithoutFlags = append(criteria.WithoutFlags, imap.SeenFlag)
	}
	uids, err := cl.Search(criteria)
	if err != nil {
		return err
	}
	seqset := new(imap.SeqSet)
	if len(uids) > 0 {
		sort.Slice(uids, func(i int, j int) bool {
			return uids[i] > uids[j]
		})
		last := int(math.Min(float64(num), float64(len(uids))))
		if num == -1 {
			last = int(mbox.Messages - 1)
		}
		uids = uids[:last]
		seqset.AddNum(uids...)

		messages := make(chan *imap.Message, 1)
		done := make(chan error, last)
		go func() {
			done <- cl.Fetch(seqset, []imap.FetchItem{imap.FetchEnvelope, imap.FetchFlags}, messages)
		}()

		if ia {
			res, err := util.AskFzf(func(stdin io.WriteCloser) {
				for msg := range messages {
					stdin.Write([]byte(fmt.Sprintf("%d~%s\n", msg.SeqNum, msg.Envelope.Subject)))
				}
			}, []string{"--with-nth=2..", "--delimiter=~",
				fmt.Sprintf("--header=Select mail message from %s", credname),
				fmt.Sprintf("--prompt=%s", util.PromptString())})
			if err != nil {
				return err
			}
			if res != "" {
				parts := strings.Split(res, "~")
				err := App.RunCommand([]string{"mail", "show", "-n", credname, "-m", parts[0]})
				if err != nil {
					return err
				}
			}
		} else {
			for msg := range messages {
				if filter == "" || (filter != "" && strings.Contains(msg.Envelope.Subject, filter)) {
					fmt.Println(formatMessageLine(msg))
				}
			}
		}

		if err := <-done; err != nil {
			return err
		}
	} else {
		util.PrintInfo("No unread messages")
	}
	return nil
}

func mailGetMessageBody(cl *client.Client, seqSet *imap.SeqSet) error {
	var section imap.BodySectionName
	items := []imap.FetchItem{section.FetchItem()}
	messages := make(chan *imap.Message, 1)
	go func() {
		if err := cl.Fetch(seqSet, items, messages); err != nil {
			util.PrintError(err)
		}
	}()
	msg := <-messages
	if msg == nil {
		return fmt.Errorf("Server didn't returned message")
	}

	r := msg.GetBody(&section)
	if r == nil {
		return fmt.Errorf("Server didn't returned message body")
	}
	mr, err := mail.CreateReader(r)
	if err != nil {
		return err
	}
	header := mr.Header
	if date, err := header.Date(); err == nil {
		util.PrintInfo("Date:", date)
	}
	if from, err := header.AddressList("From"); err == nil {
		util.PrintInfo("From:", from)
	}
	if to, err := header.AddressList("To"); err == nil {
		util.PrintInfo("To:", to)
	}
	if subject, err := header.Subject(); err == nil {
		util.PrintInfo("Subject:", subject)
	}
	for {
		p, err := mr.NextPart()
		if err != nil {
			if !strings.Contains(err.Error(), "unhandled charset") {
				break
			}
		}
		switch h := p.Header.(type) {
		case *mail.InlineHeader:
			mime := p.Header.Get("content-type")
			b, _ := ioutil.ReadAll(p.Body)
			if strings.HasPrefix(mime, "text/plain") {
				fmt.Println(string(b))
			} else if strings.HasPrefix(mime, "text/html") {
				fmt.Println(util.HtmlToAnsi(string(b)))
			} else {
				fmt.Printf("%s content", mime)
			}
		case *mail.AttachmentHeader:
			filename, _ := h.Filename()
			util.PrintInfo("Got attachment", filename)
		default:
			util.PrintInfo(fmt.Sprintf("%v", h))
		}
	}
	return nil
}

func mailGetCounts(mbox *imap.MailboxStatus, cl *client.Client) (int, int) {
	criteria := imap.NewSearchCriteria()
	criteria.WithoutFlags = []string{imap.SeenFlag, imap.DeletedFlag}
	uids, err := cl.Search(criteria)
	if err != nil {
		util.PrintError(err)
		return 0, 0
	}
	return len(uids), int(mbox.Messages)
}

func mailDeleteMessage(cl *client.Client, seqSet *imap.SeqSet, purge bool) error {
	items := []imap.FetchItem{imap.FetchEnvelope}
	messages := make(chan *imap.Message, 1)
	go func() {
		if err := cl.Fetch(seqSet, items, messages); err != nil {
			util.PrintError(err)
		}
	}()
	flags := []interface{}{imap.DeletedFlag}
	for msg := range messages {
		if msg != nil {
			conf := fmt.Sprintf("Delete %v?", a.BrightMagenta(msg.Envelope.Subject))
			if purge {
				conf = fmt.Sprintf("Delete permanently %v?", a.BrightMagenta(msg.Envelope.Subject))
			}
			if util.AskConfirm(conf) {
				err := cl.Store(seqSet, "+FLAGS.SILENT", flags, nil)
				if err != nil {
					return err
				}
				if purge {
					err = cl.Expunge(nil)
					if err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}
