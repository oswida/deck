package cmd

import (
	"deck/util"
	"fmt"
	"strings"

	"github.com/desertbit/grumble"
	"github.com/jedib0t/go-pretty/table"
	"github.com/k0kubun/pp"
)

func init() {
	creds := &grumble.Command{
		Name:    "creds",
		Help:    "security credentials",
		Aliases: []string{"cr"},
		Usage:   "creds",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "credential name (required)")
			f.Bool("i", "interactive", false, "use interactive mode")
		},
		AllowArgs: false,
		Run:       runCred,
	}
	App.AddCommand(creds)
	cradd := &grumble.Command{
		Name:      "add",
		Help:      "add credential",
		Aliases:   []string{},
		Usage:     "add",
		AllowArgs: false,
		Run:       addCred,
	}
	creds.AddCommand(cradd)
	crdel := &grumble.Command{
		Name:      "del",
		Help:      "delete credential",
		Aliases:   []string{},
		Usage:     "del",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "credential name (required)")
		},
		Completer: util.CredComplete,
		Run:       delCred,
	}
	creds.AddCommand(crdel)
	credit := &grumble.Command{
		Name:      "edit",
		Help:      "edit credential",
		Aliases:   []string{},
		Usage:     "edit",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "credential name (required)")
		},
		Completer: util.CredComplete,
		Run:       editCred,
	}
	creds.AddCommand(credit)
	crident := &grumble.Command{
		Name:      "ident",
		Help:      "change credential identifier",
		Aliases:   []string{},
		Usage:     "ident",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "credential name (required)")
		},
		Completer: util.CredComplete,
		Run:       identCred,
	}
	creds.AddCommand(crident)
	crsecret := &grumble.Command{
		Name:      "secret",
		Help:      "change credential secret",
		Aliases:   []string{},
		Usage:     "secret",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "credential name (required)")
		},
		Completer: util.CredComplete,
		Run:       secretCred,
	}
	creds.AddCommand(crsecret)
}

func determineName(c *grumble.Context) string {
	nm := c.Flags.String("name")
	if nm != "" {
		return nm
	}
	if c.Flags.Bool("interactive") {
		var creds []util.DbCredential
		err := util.Db.All(&creds)
		if err != nil {
			return ""
		}
		options := []string{}
		for _, cr := range creds {
			options = append(options, cr.Name)
		}
		return util.AskSelectWithCancel("Credential: ", options)
	}
	return ""
}

func doDeleteCred(cc *util.DbCredential) error {
	pp.Print(cc)
	doit := util.AskConfirm("Delete?")
	if doit {
		err := util.Db.DeleteStruct(cc)
		if err != nil {
			return err
		} else {
			util.PrintSuccess("credential deleted")
		}
	}
	return nil
}

func doEditCred(cc *util.DbCredential) {
	mkey := util.GetMasterKey(App)
	cret := util.AskCredential(mkey, cc)
	pp.Print(cret)
	doit := util.AskConfirm("Save?")
	if doit {
		err := util.Db.Save(&cret)
		if err != nil {
			util.PrintError(err)
		}
	}
}

func doRecryptCred(cc *util.DbCredential, field string) {
	if field != "ident" && field != "secret" {
		return
	}
	mkey := util.GetMasterKey(App)
	parts := strings.Split(cc.Ident, "$")
	_, err := util.Decrypt(mkey, parts[0], parts[1])
	if err != nil {
		util.PrintError("Incorrect decrypt key")
		return
	}
	pwd := util.AskPassword("New ident")
	if pwd != "" {
		nonce, enc := util.Encrypt(mkey, pwd)
		switch field {
		case "ident":
			cc.Ident = nonce + "$" + enc
		case "secret":
			cc.Secret = nonce + "$" + enc
		}
		err = util.Db.Save(cc)
		if err != nil {
			util.PrintError(err)
		} else {
			util.PrintSuccess("ident changed")
		}
	}
}

func runCred(c *grumble.Context) error {
	if c.Flags.Bool("interactive") {
		nm := determineName(c)
		var cc util.DbCredential
		err := util.Db.One("Name", nm, &cc)
		if err != nil {
			return err
		}
		action := util.AskSelectWithCancel("Action:",
			[]string{"edit", "delete", "change ident", "change secret"})
		switch action {
		case "delete":
			err = doDeleteCred(&cc)
			if err != nil {
				return err
			}
		case "edit":
			doEditCred(&cc)
		case "change ident":
			doRecryptCred(&cc, "ident")
		case "change secret":
			doRecryptCred(&cc, "secret")
		}
	} else {
		var creds []util.DbCredential
		err := util.Db.All(&creds)
		if err != nil {
			return err
		}
		var rows []table.Row
		for _, cr := range creds {
			rows = append(rows, table.Row{cr.Name, cr.Type, cr.Location})
		}
		util.PrintTable("Credential summary", table.Row{"Name", "Type", "Location"}, rows, true)
	}
	return nil
}

func findCred(c *grumble.Context) (*util.DbCredential, error) {
	name := determineName(c)
	if name == "" {
		return nil, fmt.Errorf("missing credential name")
	}
	var cc util.DbCredential
	err := util.Db.One("Name", name, &cc)
	if err != nil {
		return nil, err
	}
	return &cc, nil
}

func addCred(c *grumble.Context) error {
	mkey := util.GetMasterKey(App)
	cred := util.AskCredential(mkey, nil)
	pp.Print(cred)
	save := util.AskConfirm("Save?")
	if save {
		err := util.Db.Save(&cred)
		if err != nil {
			return err
		}
	}
	return nil
}

func editCred(c *grumble.Context) error {
	cc, err := findCred(c)
	if err != nil {
		return err
	}
	doEditCred(cc)
	return nil
}

func delCred(c *grumble.Context) error {
	cc, err := findCred(c)
	if err != nil {
		return err
	}
	err = doDeleteCred(cc)
	if err != nil {
		return err
	}
	return nil
}

func identCred(c *grumble.Context) error {
	cc, err := findCred(c)
	if err != nil {
		return err
	}
	doRecryptCred(cc, "ident")
	return nil
}

func secretCred(c *grumble.Context) error {
	cc, err := findCred(c)
	if err != nil {
		return err
	}
	doRecryptCred(cc, "secret")
	return nil
}
