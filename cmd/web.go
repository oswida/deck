package cmd

import (
	"deck/util"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/desertbit/grumble"
)

func init() {
	mk := &grumble.Command{
		Name:      "web",
		Help:      "show web page",
		Aliases:   []string{},
		Usage:     "web <page uri>",
		AllowArgs: true,
		Run:       webRun,
	}
	App.AddCommand(mk)
}

func webRun(c *grumble.Context) error {
	if len(c.Args) > 0 {
		resp, err := http.Get(c.Args[0])
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		html := string(body)
		fmt.Println(util.HtmlToAnsi(html))
	} else {
		return fmt.Errorf("Page UIR required")
	}
	return nil
}
