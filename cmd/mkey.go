package cmd

import (
	"deck/util"
	"fmt"

	"github.com/desertbit/grumble"
)

func init() {
	mk := &grumble.Command{
		Name:      "mkey",
		Help:      "master encryption key support",
		Aliases:   []string{"mk"},
		Usage:     "mkey ins|del",
		AllowArgs: true,
		Run:       mkeyRun,
	}
	App.AddCommand(mk)
}

func mkeyRun(c *grumble.Context) error {
	if len(c.Args) <= 0 {
		return fmt.Errorf("missing mkey operation")
	}
	switch c.Args[0] {
	case "ins":
		if util.MasterKey != nil {
			return fmt.Errorf("encryption master key already present")
		}
		_ = util.GetMasterKey(App)
	case "del":
		util.MasterKey = nil
		App.SetPrompt(util.PromptString())
	}
	return nil
}
