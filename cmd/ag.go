package cmd

import (
	"deck/util"
	"fmt"
	"io"
	"os/exec"
	"strings"

	"github.com/desertbit/grumble"
	"github.com/skratchdot/open-golang/open"
)

func init() {
	App.AddCommand(&grumble.Command{
		Name:    "find",
		Help:    "find files with a given text (requires ag - silver searcher)",
		Aliases: []string{"fd"},
		Usage:   "find <search text>",
		Flags: func(f *grumble.Flags) {
			f.Bool("i", "case insensitive", false, "case insensitive search")
			f.Bool("w", "words", false, "match whole words")
		},
		AllowArgs: true,
		Run:       runAg,
	})

}

func runAg(c *grumble.Context) error {
	cargs := []string{"-l", "-m 1", "--silent"}
	if c.Flags.Bool("case insensitive") {
		cargs = append(cargs, "-i")
	}
	if c.Flags.Bool("words") {
		cargs = append(cargs, "-w")
	}
	if len(c.Args) > 0 {
		text := strings.TrimSpace(c.Args[0])
		var wslist []util.DbWorkspace
		err := util.Db.All(&wslist)
		if err != nil {
			return err
		}

		sel, err := util.AskFzf(func(stdin io.WriteCloser) {
			for _, w := range wslist {
				callargs := []string{text}
				callargs = append(callargs, w.Path)
				callargs = append(callargs, cargs...)
				cmd := exec.Command("ag", callargs...)
				stdout, err := cmd.StdoutPipe()
				if err != nil {
					util.PrintError(err)
				}
				err = cmd.Start()
				if err != nil {
					util.PrintError(err)
				}
				go func() {
					var buff = make([]byte, 1024)
					for {
						n, err := stdout.Read(buff)
						if err != nil {
							break
						}
						stdin.Write(buff[:n])
						stdin.Write([]byte("\n"))
					}
				}()
				cmd.Wait()
			}
		}, []string{
			fmt.Sprintf("--header=Search for '%s' in workspaces", c.Args[0]),
			fmt.Sprintf("--prompt=%s", util.PromptString())})
		if err != nil {
			return err
		}
		if sel != "" {
			err := open.Run(sel)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
