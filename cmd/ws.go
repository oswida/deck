package cmd

import (
	"deck/util"
	"io"

	"github.com/desertbit/grumble"
	"github.com/jedib0t/go-pretty/table"
	"github.com/skratchdot/open-golang/open"
)

func init() {
	App.AddCommand(&grumble.Command{
		Name:      "workspace",
		Help:      "workspaces info and files",
		Aliases:   []string{"ws"},
		Usage:     "workspaces",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "workspace name")
			f.Bool("i", "interactive", false, "interactive mode")
		},
		Run: runWs,
	})
}

func runWs(c *grumble.Context) error {
	var wslist []util.DbWorkspace
	err := util.Db.All(&wslist)
	if err != nil {
		return err
	}
	if c.Flags.Bool("interactive") {
		sel, err := util.AskFzf(func(stdin io.WriteCloser) {
			for _, ww := range wslist {
				util.ListFilesRecursively(ww.Path, func(line string) {
					stdin.Write([]byte(line + "\n"))
				})
			}
		}, []string{})
		if err != nil {
			return err
		}
		if sel != "" {
			err := open.Run(sel)
			if err != nil {
				return err
			}
		}
	} else {
		rows := []table.Row{}
		for _, ww := range wslist {
			rows = append(rows, table.Row{ww.Name, ww.Path})
		}
		util.PrintTable("Workspace summary", table.Row{"Name", "Path"}, rows, false)
	}
	return nil
}
