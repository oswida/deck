package cmd

import "github.com/desertbit/grumble"

var (
	App *grumble.App = grumble.New(&grumble.Config{
		HistoryFile: "/tmp/deck.history",
		Name:        "deck",
		Description: "Information space deck",
	})
	CurrentAppMode = ""
)
