package cmd

import (
	"deck/util"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/JamesClonk/go-todotxt"
	"github.com/desertbit/grumble"
	"github.com/jedib0t/go-pretty/table"
	a "github.com/logrusorgru/aurora"
)

var LastTodoFile = ""

func init() {
	td := &grumble.Command{
		Name:      "todo",
		Help:      "todo management (todo.txt format)",
		Aliases:   []string{"td"},
		Usage:     "todo [<flag> | <subcommand>]",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("s", "sort", "", "sort todo entries by selected criteria: prio(rity), creo(ated), due, comp(leted)")
			f.String("f", "filter", "", "filter todo entries according some criteria: comp(leted), <project or context name>")
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
		},
		Run: runTodo,
	}
	App.AddCommand(td)
	tda := &grumble.Command{
		Name:      "add",
		Help:      "add new task",
		Aliases:   []string{"a"},
		Usage:     "add <name flag>",
		AllowArgs: false,
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
		},
		Run: runAddTodo,
	}
	td.AddCommand(tda)
	tdt := &grumble.Command{
		Name:    "toggle",
		Help:    "toggle task state",
		Aliases: []string{"tg"},
		Usage:   "toggle <name flag> <task id flag>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
			f.Int("t", "task", -1, "task number")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("toggle complete", c)
		},
	}
	td.AddCommand(tdt)
	tdu := &grumble.Command{
		Name:    "due",
		Help:    "set due to date",
		Aliases: []string{},
		Usage:   "due <name flag> <task id flag>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
			f.Int("t", "task", -1, "task number")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("set due date", c)
		},
	}
	td.AddCommand(tdu)
	tdp := &grumble.Command{
		Name:    "priority",
		Help:    "set task priority",
		Aliases: []string{"prio"},
		Usage:   "priority <todo filename> <task id>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
			f.Int("t", "task", -1, "task number")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("priority", c)
		},
	}
	td.AddCommand(tdp)
	tdc := &grumble.Command{
		Name:    "contexts",
		Help:    "edit task contexts",
		Aliases: []string{"ctx"},
		Usage:   "contexts <todo filename> <task id>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
			f.Int("t", "task", -1, "task number")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("contexts", c)
		},
	}
	td.AddCommand(tdc)
	tdr := &grumble.Command{
		Name:    "projects",
		Help:    "edit task projects",
		Aliases: []string{"prj"},
		Usage:   "projects <todo filename> <task id>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
			f.Int("t", "task", -1, "task number")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("projects", c)
		},
	}
	td.AddCommand(tdr)
	tde := &grumble.Command{
		Name:    "edit",
		Help:    "edit task line",
		Aliases: []string{"ed"},
		Usage:   "edit <todo filename> <task id>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
			f.Int("t", "task", -1, "task number")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("edit line", c)
		},
	}
	td.AddCommand(tde)
	tdn := &grumble.Command{
		Name:    "content",
		Help:    "edit task content",
		Aliases: []string{"cn"},
		Usage:   "content <name flag> <task id flag>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
			f.Int("t", "task", -1, "task number")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("edit content", c)
		},
	}
	td.AddCommand(tdn)
	tdd := &grumble.Command{
		Name:    "delete",
		Help:    "delete task",
		Aliases: []string{"del"},
		Usage:   "delete <todo filename> <task id>",
		Flags: func(f *grumble.Flags) {
			f.String("n", "name", "", "todo entry name")
			f.Bool("i", "interactive", false, "use interactive mode")
			f.Bool("l", "last", false, "run action on last opened todo file")
		},
		AllowArgs: false,
		Completer: util.TodoComplete,
		Run: func(c *grumble.Context) error {
			return runTodoAction("delete", c)
		},
	}
	td.AddCommand(tdd)
}

func determineTodoFile(c *grumble.Context) string {
	nm := c.Flags.String("name")
	ia := c.Flags.Bool("interactive")
	if nm != "" {
		tt := util.FindTodoByName(nm)
		if tt != nil {
			LastTodoFile = tt.Path
			return tt.Path
		}
	} else {
		var fl string
		if c.Flags.Bool("last") && LastTodoFile != "" {
			fl = LastTodoFile
		} else if ia {
			fl = selectTodoFile()
		}
		return fl
	}
	return ""
}

func runTodo(c *grumble.Context) error {
	sort := c.Flags.String("sort")
	filter := c.Flags.String("filter")
	ia := c.Flags.Bool("interactive")
	fl := determineTodoFile(c)
	if fl != "" {
		processTodoList(fl, sort, filter, ia)
	} else {
		var todos []util.DbTodo
		err := util.Db.All(&todos)
		if err != nil {
			return err
		}
		rows := []table.Row{}
		for _, td := range todos {
			tasks, err := todotxt.LoadFromFilename(td.Path)
			if err != nil {
				return err
			}
			done := 0
			for _, tt := range tasks {
				if tt.Completed {
					done++
				}
			}
			rows = append(rows, table.Row{
				td.Name,
				td.Path,
				a.BrightCyan(len(tasks)),
				a.BrightCyan(done)})
		}
		util.PrintTable("Todo Summary", table.Row{"Name", "Path", "Total", "Completed"}, rows, true)
		fmt.Println()
	}
	return nil
}

func runAddTodo(c *grumble.Context) error {
	fl := determineTodoFile(c)
	if fl != "" {
		tasklist, err := todotxt.LoadFromFilename(fl)
		if err != nil {
			return err
		}
		line := util.AskString("Task line: ", "")
		if line != "" {
			tsk, err := todotxt.ParseTask(line)
			if err != nil {
				return err
			}
			tasklist.AddTask(tsk)
			err = tasklist.WriteToFilename(fl)
			if err != nil {
				util.PrintError(err)
			}
			util.PrintSuccess("Added: ", line)
		}
	}
	return nil
}

func runTodoAction(action string, c *grumble.Context) error {
	fl := determineTodoFile(c)
	id := c.Flags.Int("task")
	var err error
	tasks, err := todotxt.LoadFromFilename(fl)
	if err != nil {
		return err
	}
	return taskAction(action, fl, &tasks, id)
}

func selectTodoFile() string {
	var todos []util.DbTodo
	err := util.Db.All(&todos)
	if err != nil {
		util.PrintError(err)
	}
	if len(todos) <= 0 {
		util.PrintInfo("todo file list is empty")
		return ""
	}
	options := []string{}
	for _, t := range todos {
		options = append(options, t.Name)
	}
	sel := util.AskSelectWithCancel("Todo file: ", options)
	if sel != "" {
		tt := util.FindTodoByName(sel)
		if tt != nil {
			LastTodoFile = tt.Path
			return LastTodoFile
		}
	}
	return ""
}

func taskLine(task todotxt.Task) string {
	todo := a.White(task.Todo)
	csign := a.Green("")
	if task.Completed {
		csign = a.Green("✔")
		todo = a.BrightBlack(task.Todo)
	}
	prio := ""
	if task.HasPriority() {
		prio = " [" + task.Priority + "]"
	}
	created := ""
	if task.HasCreatedDate() {
		tt := task.CreatedDate
		created = fmt.Sprintf(" %d-%.2d-%.2d ", tt.Year(), tt.Month(), tt.Day())
	}
	completed := ""
	if task.HasCompletedDate() {
		tt := task.CompletedDate
		completed = fmt.Sprintf(" %d-%.2d-%.2d ", tt.Year(), tt.Month(), tt.Day())
	}
	projects := ""
	if len(task.Projects) > 0 {
		projects = strings.Join(task.Projects, " ")
	}
	ctx := ""
	if len(task.Contexts) > 0 {
		ctx = strings.Join(task.Contexts, " ")
	}
	due := ""
	if task.HasDueDate() {
		due = "due:" + task.DueDate.Format("2006-01-02")
	}
	return fmt.Sprintf("%d) %s%s%s %s %s %s %s %v",
		task.Id,
		a.BrightYellow(prio),
		a.BrightBlack(created),
		a.BrightBlack(completed),
		todo,
		a.BrightCyan(projects),
		a.BrightBlue(ctx),
		func() interface{} {
			if task.IsOverdue() {
				return a.Red(due)
			} else {
				return a.Yellow(due)
			}
		}(),
		csign)
}

func processTodoList(filename string, sort string, filter string, interactive bool) {
	fmt.Println()
	lines, tasks := processTodoFile(filename, sort, filter)
	if len(lines) > 0 {
		if interactive {
			tfl := util.AskSelect("Task: ", lines)
			if tfl != "" {
				parts := strings.Split(tfl, ")")
				if len(parts) > 1 {
					i, err := strconv.Atoi(strings.TrimSpace(parts[0]))
					if err == nil {
						action := util.AskSelectWithCancel("Action: ",
							[]string{"toggle complete", "edit content", "edit line",
								"projects", "contexts", "priority", "set due date", "delete"})
						err = taskAction(action, filename, tasks, i)
						if err != nil {
							util.PrintError(err)
						}
					} else {
						util.PrintError(err)
					}
				}
			}
		} else {
			for _, ln := range lines {
				fmt.Println(ln)
			}
			fmt.Println()
		}
	}
}

func processTodoFile(path string, sort string, filter string) ([]string, *todotxt.TaskList) {
	tasklist, err := todotxt.LoadFromFilename(path)
	if err != nil {
		util.PrintError(err)
		return []string{}, &todotxt.TaskList{}
	}
	if strings.TrimSpace(sort) != "" {
		switch sort {
		case "priority", "prio":
			err = tasklist.Sort(todotxt.SORT_PRIORITY_ASC)
			if err != nil {
				util.PrintError(err)
			}
		case "priority_d", "prio_d":
			err = tasklist.Sort(todotxt.SORT_PRIORITY_DESC)
			if err != nil {
				util.PrintError(err)
			}
		case "created", "creo":
			err = tasklist.Sort(todotxt.SORT_CREATED_DATE_ASC)
			if err != nil {
				util.PrintError(err)
			}
		case "created_d", "creo_d":
			err = tasklist.Sort(todotxt.SORT_CREATED_DATE_DESC)
			if err != nil {
				util.PrintError(err)
			}
		case "completed", "comp":
			err = tasklist.Sort(todotxt.SORT_COMPLETED_DATE_ASC)
			if err != nil {
				util.PrintError(err)
			}
		case "completed_d", "comp_d":
			err = tasklist.Sort(todotxt.SORT_COMPLETED_DATE_DESC)
			if err != nil {
				util.PrintError(err)
			}
		case "due":
			err = tasklist.Sort(todotxt.SORT_DUE_DATE_ASC)
			if err != nil {
				util.PrintError(err)
			}
		case "due_d":
			err = tasklist.Sort(todotxt.SORT_DUE_DATE_ASC)
			if err != nil {
				util.PrintError(err)
			}
		}
	}
	finaltasks := &tasklist
	if strings.TrimSpace(filter) != "" {
		finaltasks = tasklist.Filter(func(t todotxt.Task) bool {
			switch filter {
			case "completed", "comp":
				return t.Completed
			default:
				return util.ContainsString(t.Projects, filter) || util.ContainsString(t.Contexts, filter)
			}
		})
	}
	lines := []string{}
	for _, tsk := range *finaltasks {
		lines = append(lines, taskLine(tsk))
	}
	return lines, finaltasks
}

func taskAction(action string, filename string, tasks *todotxt.TaskList, id int) error {
	tsk, _ := tasks.GetTask(id)
	switch action {
	case "toggle complete":
		if tsk.Completed {
			tsk.Completed = false
			tsk.CompletedDate = time.Time{}
		} else {
			tsk.Complete()
		}
		err := tasks.WriteToFilename(filename)
		if err != nil {
			return err
		}
		util.PrintSuccess("Todo complete toggled: ", tsk.Todo, " => ", tsk.Completed)
	case "edit content":
		txt := util.AskString("Task body: ", tsk.Todo)
		tsk.Todo = txt
		err := tasks.WriteToFilename(filename)
		if err != nil {
			return err
		}
		util.PrintSuccess("Todo content change: " + tsk.Todo)
	case "edit line":
		txt := util.AskEditor("Task line: ", tsk.String())
		tt, err := todotxt.ParseTask(txt)
		if err != nil {
			return err
		} else {
			err = tasks.RemoveTask(*tsk)
			if err != nil {
				return err
			}
			tasks.AddTask(tt)
			err = tasks.WriteToFilename(filename)
			if err != nil {
				return err
			}
		}
	case "projects":
		txt := util.AskString("Task projects: ", strings.Join(tsk.Projects, " "))
		tsk.Projects = strings.Split(txt, " ")
		err := tasks.WriteToFilename(filename)
		if err != nil {
			return err
		}
		util.PrintSuccess("Todo projects updated: " + tsk.Todo + " => " + strings.Join(tsk.Projects, ","))
	case "contexts":
		txt := util.AskString("Task contexts: ", strings.Join(tsk.Contexts, " "))
		tsk.Contexts = strings.Split(txt, " ")
		err := tasks.WriteToFilename(filename)
		if err != nil {
			return err
		}
		util.PrintSuccess("Todo contexts updated: " + tsk.Todo + " => " + strings.Join(tsk.Contexts, ","))
	case "priority":
		txt := util.AskString("Task priority: ", tsk.Priority)
		tsk.Priority = txt
		err := tasks.WriteToFilename(filename)
		if err != nil {
			return err
		}
		util.PrintSuccess("Todo priority set: " + tsk.Todo + " => " + tsk.Priority)
	case "set due date":
		txt := util.AskString("Task due date YYY-MM-DD: ", tsk.DueDate.Format("2006-01-02"))
		tsk.DueDate, _ = time.Parse("2006-01-02", txt)
		err := tasks.WriteToFilename(filename)
		if err != nil {
			return err
		}
		util.PrintSuccess("Todo due date set: " + tsk.Todo + " => " + tsk.DueDate.Format("2006-02-01"))
	case "delete":
		if util.AskConfirm(fmt.Sprintf("%v %s?", a.Red("Delete"), tsk.Todo)) {
			err := tasks.RemoveTask(*tsk)
			if err != nil {
				return err
			}
			err = tasks.WriteToFilename(filename)
			if err != nil {
				return err
			}
			util.PrintSuccess("Todo deleted: " + tsk.Todo)
		}
	}
	return nil
}
