package util

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"github.com/logrusorgru/aurora"
	"gopkg.in/AlecAivazis/survey.v1"
)

func AskConfirm(msg string) bool {
	var retv bool
	prompt := &survey.Confirm{
		Message: msg,
	}
	err := survey.AskOne(prompt, &retv, nil)
	if err != nil {
		PrintError(err)
		return false
	}
	return retv
}

func AskString(msg string, def string) string {
	var retv string
	prompt := &survey.Input{
		Message: msg,
		Default: def,
	}
	err := survey.AskOne(prompt, &retv, nil)
	if err != nil {
		PrintError(err)
	}
	return retv
}

func AskEditor(msg string, def string) string {
	var retv string
	prompt := &survey.Editor{
		Message:       msg,
		Default:       def,
		AppendDefault: true,
	}
	err := survey.AskOne(prompt, &retv, nil)
	if err != nil {
		PrintError(err)
	}
	return retv
}

func AskPassword(msg string) string {
	var retv string
	prompt := &survey.Password{
		Message: msg,
	}
	err := survey.AskOne(prompt, &retv, nil)
	if err != nil {
		PrintError(err)
	}
	return retv
}

func AskSelect(msg string, items []string) string {
	var retv string
	prompt := &survey.Select{
		Message: msg,
		Options: items,
	}
	err := survey.AskOne(prompt, &retv, nil)
	if err != nil {
		PrintError(err)
	}
	return retv
}

func AskSelectWithCancel(msg string, items []string) string {
	var retv string
	cancelStr := fmt.Sprintf("%v", aurora.BrightRed("cancel"))
	options := []string{cancelStr}
	for _, it := range items {
		options = append(options, it)
	}
	prompt := &survey.Select{
		Message: msg,
		Options: options,
	}
	err := survey.AskOne(prompt, &retv, nil)
	if err != nil {
		PrintError(err)
	}
	if retv == cancelStr {
		retv = ""
	}
	return retv
}

func AskCredential(mkey string, def *DbCredential) DbCredential {
	// mkey := GetMasterKey()
	retv := DbCredential{
		Name:     "",
		Type:     "",
		Location: "",
		Ident:    "",
		Secret:   "",
	}
	if def != nil {
		retv.ID = def.ID
		retv.Name = def.Name
		retv.Type = def.Type
		retv.Location = def.Location
		retv.Ident = def.Ident
		retv.Secret = def.Secret
	}
	var credQs = []*survey.Question{
		{
			Name: "Name",
			Prompt: &survey.Input{
				Message: "Name:",
				Default: retv.Name,
			},
			Validate: survey.Required,
		},
		{
			Name: "Type",
			Prompt: &survey.Select{
				Message: "Type:",
				Options: []string{"mail"},
				Default: retv.Type,
			},
			Validate: survey.Required,
		},
		{
			Name: "Location",
			Prompt: &survey.Input{
				Message: "Location:",
				Default: retv.Location,
			},
			Validate: survey.Required,
		},
	}
	if def == nil {
		credQs = append(credQs, &survey.Question{
			Name: "Ident",
			Prompt: &survey.Password{
				Message: "Ident:",
			},
		})
		credQs = append(credQs, &survey.Question{
			Name: "Secret",
			Prompt: &survey.Password{
				Message: "Secret:",
			},
		})
	}
	err := survey.Ask(credQs, &retv)
	if err != nil {
		PrintError(err)
	}
	if def == nil {
		var n1, n2, e1, e2 string
		n1, e1 = Encrypt(mkey, retv.Ident)
		n2, e2 = Encrypt(mkey, retv.Secret)
		retv.Ident = n1 + "$" + e1
		retv.Secret = n2 + "$" + e2
	}
	return retv
}

func AskFzf(dataFunc func(io.WriteCloser), options []string) (string, error) {
	var result strings.Builder
	opt := []string{"-e"}
	opt = append(opt, options...)
	cmd := exec.Command("fzf", opt...)
	cmd.Stdout = &result
	cmd.Stderr = os.Stderr
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return "", err
	}
	err = cmd.Start()
	if err != nil {
		return "", err
	}

	go func() {
		dataFunc(stdin)
		stdin.Close()
	}()

	err = cmd.Wait()
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(result.String()), nil
}
