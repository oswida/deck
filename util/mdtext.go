package util

import (
	"fmt"
	"io"
	"strings"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	"github.com/gomarkdown/markdown/parser"
	"github.com/logrusorgru/aurora"
	"github.com/lunny/html2md"
)

var (
	OrderedList bool
	OrderedNum  int
	Indent      bool
)

type AnsiRenderer struct {
}

func (r AnsiRenderer) RenderNode(w io.Writer, node ast.Node, entering bool) ast.WalkStatus {
	switch node := node.(type) {
	case *ast.Text:
		rtxt := string(node.Literal)
		if Indent {
			rtxt = "  " + strings.ReplaceAll(rtxt, "\n", "\n  ")
		}
		w.Write([]byte(rtxt))
	case *ast.Softbreak:
		w.Write([]byte("\n"))
	case *ast.Hardbreak:
		w.Write([]byte("\n"))
	case *ast.NonBlockingSpace:
		w.Write([]byte(" "))
	case *ast.Emph:
		if entering {
			w.Write([]byte("\033[3m"))
		} else {
			w.Write([]byte("\033[0m"))
		}
	case *ast.Strong:
		if entering {
			w.Write([]byte("\033[1m"))
		} else {
			w.Write([]byte("\033[0m"))
		}
	case *ast.Del:
		if entering {
			w.Write([]byte("\033[2m"))
		} else {
			w.Write([]byte("\033[0m"))
		}
	case *ast.BlockQuote:
		if entering {
			w.Write([]byte("\n"))
			w.Write([]byte("\033[2m"))
			Indent = true
		} else {
			w.Write([]byte("\033[0m"))
			w.Write([]byte("\n"))
			Indent = false
		}
	case *ast.Aside:
		w.Write([]byte("<aside>"))
	case *ast.Link:
		if entering {
			w.Write([]byte("\033]8;;"))
			w.Write(node.Destination)
			w.Write([]byte("\007"))
		} else {
			w.Write([]byte("\033]8;;\007\033[0m"))
		}
	case *ast.Citation:
		if entering {
			w.Write([]byte("\n"))
			w.Write([]byte("\033[2m"))
			Indent = true
		} else {
			w.Write([]byte("\033[0m"))
			w.Write([]byte("\n"))
			Indent = false
		}
	case *ast.Image:
		w.Write([]byte(" ▣ "))
	case *ast.Code:
		txt := fmt.Sprintf("%v", aurora.Green(string(node.Literal)))
		w.Write([]byte(txt))
	case *ast.CodeBlock:
		txt := fmt.Sprintf("%v", aurora.Magenta(string(node.Literal)))
		w.Write([]byte(txt))
	case *ast.Caption:
		if entering {
			w.Write([]byte("\n"))
			w.Write([]byte("\033[42m"))
			w.Write([]byte(" "))
			w.Write([]byte("\033[37m"))
		} else {
			w.Write([]byte(" "))
			w.Write([]byte("\033[0m"))
			w.Write([]byte("\n"))
		}
	case *ast.Heading:
		if entering {
			w.Write([]byte("\n"))
			w.Write([]byte("\033[44m"))
			w.Write([]byte(" "))
			w.Write([]byte("\033[37m"))
		} else {
			w.Write([]byte(" "))
			w.Write([]byte("\033[0m"))
			w.Write([]byte("\n"))
		}
	case *ast.HorizontalRule:
		txt := fmt.Sprintf("%v", aurora.Yellow("────────────────────"))
		w.Write([]byte(txt))
	case *ast.List:
		w.Write([]byte("\n"))
		if entering {
			if node.ListFlags&ast.ListTypeOrdered != 0 {
				OrderedList = true
				OrderedNum = 1
			} else {
				OrderedList = false
			}
		} else {
			OrderedList = false
			OrderedNum = 0
		}
	case *ast.ListItem:
		if entering {
			if OrderedList {
				w.Write([]byte(fmt.Sprintf("%v. ", aurora.Yellow(OrderedNum))))
				OrderedNum++
			} else {
				txt := fmt.Sprintf("%v", aurora.Yellow("* "))
				w.Write([]byte(txt))
			}
		} else {
			w.Write([]byte("\n"))
		}
	// case *ast.Table:
	// 	tag := tagWithAttributes("<table", BlockAttrs(node))
	// 	r.outOneOfCr(w, entering, tag, "</table>")
	// case *ast.TableCell:
	// 	r.tableCell(w, node, entering)
	// case *ast.TableHeader:
	// 	r.outOneOfCr(w, entering, "<thead>", "</thead>")
	// case *ast.TableBody:
	// 	r.tableBody(w, node, entering)
	// case *ast.TableRow:
	// 	r.outOneOfCr(w, entering, "<tr>", "</tr>")
	// case *ast.TableFooter:
	// 	r.outOneOfCr(w, entering, "<tfoot>", "</tfoot>")
	// case *ast.Math:
	// 	r.outOneOf(w, true, `<span class="math inline">\(`, `\)</span>`)
	// 	EscapeHTML(w, node.Literal)
	// 	r.outOneOf(w, false, `<span class="math inline">\(`, `\)</span>`)
	// case *ast.MathBlock:
	// 	r.outOneOf(w, entering, `<p><span class="math display">\[`, `\]</span></p>`)
	// 	if entering {
	// 		EscapeHTML(w, node.Literal)
	// 	}
	// case *ast.DocumentMatter:
	// 	r.matter(w, node, entering)
	// case *ast.Callout:
	// 	r.callout(w, node)
	case *ast.Index:
		w.Write([]byte("<index>"))
		// case *ast.Paragraph:
		// 	w.Write([]byte("para: "))
		// 	r.index(w, node)
		// case *ast.Subscript:
		// 	r.outOneOf(w, true, "<sub>", "</sub>")
		// 	if entering {
		// 		Escape(w, node.Literal)
		// 	}
		// 	r.outOneOf(w, false, "<sub>", "</sub>")
		// case *ast.Superscript:
		// 	r.outOneOf(w, true, "<sup>", "</sup>")
		// 	if entering {
		// 		Escape(w, node.Literal)
		// 	}
		// 	r.outOneOf(w, false, "<sup>", "</sup>")
	}
	return ast.GoToNext
}

func (r AnsiRenderer) RenderHeader(w io.Writer, ast ast.Node) {

}

func (r AnsiRenderer) RenderFooter(w io.Writer, ast ast.Node) {

}

func MarkdownToAnsi(text string) string {
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs
	parser := parser.NewWithExtensions(extensions)
	parsed := markdown.Parse([]byte(text), parser)
	renderer := AnsiRenderer{}
	res := markdown.Render(parsed, renderer)
	return string(res)
}

func HtmlToAnsi(html string) string {
	html2md.AddConvert(func(content string) string {
		content = strings.ReplaceAll(content, "&nbsp;", " ")
		return content
	})
	md := html2md.Convert(html)
	return MarkdownToAnsi(md)
}
