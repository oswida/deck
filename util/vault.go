package util

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"strings"

	"github.com/awnumar/memguard"
	"github.com/desertbit/grumble"
	"github.com/minio/sio"
	"golang.org/x/crypto/hkdf"
	"gopkg.in/AlecAivazis/survey.v1"
)

var (
	MasterKey *memguard.Enclave
)

// return: nonce, encrypted
func Encrypt(mkey string, content string) (string, string) {
	hx := hex.EncodeToString([]byte(mkey))
	masterkey, err := hex.DecodeString(hx)
	if err != nil {
		fmt.Printf("Cannot decode hex key: %v", err) // add error handling
		return "", ""
	}

	var nonce [32]byte
	if _, err = io.ReadFull(rand.Reader, nonce[:]); err != nil {
		fmt.Printf("Failed to read random data: %v", err) // add error handling
		return "", ""
	}

	// derive an encryption key from the master key and the nonce
	var key [32]byte
	kdf := hkdf.New(sha256.New, masterkey, nonce[:], nil)
	if _, err = io.ReadFull(kdf, key[:]); err != nil {
		fmt.Printf("Failed to derive encryption key: %v", err) // add error handling
		return "", ""
	}

	input := strings.NewReader(content)
	output := bytes.NewBuffer([]byte{})

	if _, err = sio.Encrypt(output, input, sio.Config{Key: key[:]}); err != nil {
		fmt.Printf("Failed to encrypt data: %v", err) // add error handling
		return "", ""
	}
	ns := make([]byte, 32)
	for i, b := range nonce {
		ns[i] = b
	}
	return hex.EncodeToString([]byte(ns)), hex.EncodeToString(output.Bytes())
}

func DecryptString(mkey string, st string) (string, error) {
	parts := strings.Split(st, "$")
	return Decrypt(mkey, parts[0], parts[1])
}

func Decrypt(mkey string, nnc string, content string) (string, error) {
	hx := hex.EncodeToString([]byte(mkey))
	masterkey, err := hex.DecodeString(hx)
	if err != nil {
		return "", err
	}

	nonce, err := hex.DecodeString(nnc)
	if err != nil {
		return "", err
	}

	var key [32]byte
	kdf := hkdf.New(sha256.New, masterkey, nonce, nil)
	if _, err = io.ReadFull(kdf, key[:]); err != nil {
		return "", err
	}

	inp, err := hex.DecodeString(content)
	if err != nil {
		return "", err
	}

	input := bytes.NewReader(inp)
	output := bytes.NewBuffer([]byte{})

	if _, err = sio.Decrypt(output, input, sio.Config{Key: key[:]}); err != nil {
		if _, ok := err.(sio.Error); ok {
			return "", err
		}
		return "", err
	}

	return output.String(), nil
}

func GetMasterKey(app *grumble.App) string {
	if MasterKey == nil {
		var pwd string
		prompt := &survey.Password{
			Message: "Enter master key: ",
		}
		err := survey.AskOne(prompt, &pwd, nil)
		if err != nil {
			PrintError(err)
			return ""
		}
		MasterKey = memguard.NewEnclave([]byte(pwd))
		app.SetPrompt(PromptString())
	}
	mk, _ := MasterKey.Open()
	return mk.String()
}
