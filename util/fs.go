package util

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

func FileExists(filename string) bool {
	_, err := os.Stat(filename)
	return !os.IsNotExist(err)
}

func DirExists(filename string) bool {
	finfo, err := os.Stat(filename)
	return !os.IsNotExist(err) && finfo.IsDir()
}

func MakeDir(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
}

func SetDefaultConfig() {
	cdir, _ := os.UserHomeDir()
	cdir = cdir + string(os.PathSeparator) + "Dokumenty"
	viper.Set("workspaces", []string{cdir})
	viper.Set("index.ext", []string{"txt", "md", "org", "eml"})
}

func ExpandPath(path string, makeFromHome bool) string {
	retv := path
	hdir, _ := os.UserHomeDir()
	retv = strings.ReplaceAll(retv, "~", hdir)
	if makeFromHome && !filepath.IsAbs(retv) {
		retv = hdir + string(os.PathSeparator) + retv
	}
	r, _ := filepath.Abs(retv)
	return r
}

func ListFilesRecursively(root string, procFunc func(string)) ([]string, error) {
	retv := []string{}
	err := filepath.Walk(root,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			retv = append(retv, path)
			if procFunc != nil {
				procFunc(path)
			}
			return nil
		})
	if err != nil {
		return retv, err
	}
	return retv, nil
}
