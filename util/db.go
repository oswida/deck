package util

import (
	"strings"

	"github.com/asdine/storm"
	"github.com/desertbit/grumble"
)

var DbPath string
var Db *storm.DB

type DbCredential struct {
	ID       int    `storm:"id,increment"`
	Type     string `storm:"index"`
	Name     string `storm:"index,unique"`
	Location string
	Ident    string
	Secret   string
}

func InitDb() error {
	var err error
	Db, err = storm.Open(DbPath)
	if err != nil {
		return err
	}
	err = Db.ReIndex(&DbCredential{})
	// err = Db.Init(&DbCredential{})
	if err != nil {
		return err
	}
	return nil
}

func FindCredentialByName(cred string) *DbCredential {
	name := strings.TrimSpace(cred)
	var cc DbCredential
	err := Db.One("Name", name, &cc)
	if err != nil {
		return nil
	}
	return &cc
}

func ListCredentialByPrefix(pref string) *[]DbCredential {
	var cc []DbCredential
	err := Db.Prefix("Name", pref, &cc)
	if err != nil {
		return nil
	}
	return &cc
}

func ListCredentialByType(tp string) *[]DbCredential {
	var cc []DbCredential
	err := Db.Find("Type", tp, &cc)
	if err != nil {
		return nil
	}
	return &cc
}

func DecryptCredentialData(app *grumble.App, name string) (*DbCredential, error) {
	mkey := GetMasterKey(app)
	cret := FindCredentialByName(name)
	ident, err := DecryptString(mkey, cret.Ident)
	if err != nil {
		return nil, err
	}
	secret, err := DecryptString(mkey, cret.Secret)
	if err != nil {
		return nil, err
	}
	cret.Ident = ident
	cret.Secret = secret
	return cret, nil
}

type DbTodo struct {
	ID   int    `storm:"id,increment"`
	Name string `storm:"index,unique"`
	Path string
}

func FindTodoByName(td string) *DbTodo {
	name := strings.TrimSpace(td)
	var cc DbTodo
	err := Db.One("Name", name, &cc)
	if err != nil {
		return nil
	}
	return &cc
}

func ListTodoByPrefix(pref string) *[]DbTodo {
	var cc []DbTodo
	err := Db.Prefix("Name", pref, &cc)
	if err != nil {
		return nil
	}
	return &cc
}

type DbWorkspace struct {
	ID   int    `storm:"id,increment"`
	Name string `storm:"index,unique"`
	Path string
}

func FindWorkspaceByName(td string) *DbWorkspace {
	name := strings.TrimSpace(td)
	var cc DbWorkspace
	err := Db.One("Name", name, &cc)
	if err != nil {
		return nil
	}
	return &cc
}

func ListWorkspaceByPrefix(pref string) *[]DbWorkspace {
	var cc []DbWorkspace
	err := Db.Prefix("Name", pref, &cc)
	if err != nil {
		return nil
	}
	return &cc
}
