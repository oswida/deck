package util

func ContainsString(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func FindString(a []string, x string) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return len(a)
}

func RemoveString(a []string, x string) []string {
	s := FindString(a, x)
	return append(a[:s], a[s+1:]...)
}
