package util

import (
	"fmt"
	"os"
	"strings"

	"github.com/jedib0t/go-pretty/table"
	"github.com/jedib0t/go-pretty/text"
	a "github.com/logrusorgru/aurora"
)

func PrintError(msgs ...interface{}) {
	toprint := append([]interface{}{a.Red("error: ")}, msgs...)
	fmt.Println(toprint...)
}

func PrintSuccess(msgs ...interface{}) {
	toprint := append([]interface{}{a.Green("success: ")}, msgs...)
	fmt.Println(toprint...)
}

func PrintWarning(msgs ...interface{}) {
	toprint := append([]interface{}{a.Yellow("warning: ")}, msgs...)
	fmt.Println(toprint...)
}

func PrintInfo(msgs ...interface{}) {
	toprint := append([]interface{}{a.Blue("info: ")}, msgs...)
	fmt.Println(toprint...)
}

func PromptString() string {
	icons := []string{}
	if MasterKey != nil {
		icons = append(icons, "⚆")
	}
	ictext := ""
	if len(icons) > 0 {
		ictext = " " + strings.Join(icons, " ")
	}
	return fmt.Sprintf("deck%s » ", ictext)
}

func PrintTable(title interface{}, header table.Row, rows []table.Row, centered bool) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(header)
	t.SetStyle(table.StyleLight)
	t.AppendRows(rows)
	if centered {
		cfg := []table.ColumnConfig{}
		for _, h := range header {
			cfg = append(cfg, table.ColumnConfig{
				Name:  fmt.Sprintf("%v", h),
				Align: text.AlignCenter,
			})
		}
		t.SetColumnConfigs(cfg)
	}
	t.SetTitle(fmt.Sprintf("\n%v\n", title))
	t.Style().Options.DrawBorder = false
	t.Render()
	fmt.Println()
}
