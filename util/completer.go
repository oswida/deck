package util

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func fsComplete(prefix string, args []string, dirOnly bool) []string {
	rp := prefix
	if !filepath.IsAbs(rp) {
		hdir, _ := os.UserHomeDir()
		rp = hdir + string(os.PathSeparator) + rp
	}
	lastPref := ""
	if !DirExists(rp) {
		lastPref = strings.TrimSpace(filepath.Base(rp))
		rp = filepath.Dir(rp)
	}
	rp = strings.TrimSpace(rp)
	fl, err := os.Open(rp)
	if err != nil {
		return []string{}
	}
	defer fl.Close()
	files, err := fl.Readdir(-1)
	if err == nil {
		retv := []string{}
		lastPref = strings.TrimSpace(lastPref)
		for _, n := range files {
			if strings.HasPrefix(n.Name(), ".") {
				continue
			}
			if !dirOnly || (dirOnly && n.IsDir()) {
				if lastPref == "" {
					nx := n.Name()
					if !strings.HasSuffix(rp, string(os.PathSeparator)) {
						nx = "/" + nx
					}
					retv = append(retv, nx)
				} else if strings.HasPrefix(n.Name(), lastPref) {
					retv = append(retv, strings.Replace(n.Name(), lastPref, "", 1))
				}
			}
		}
		return retv
	} else {
		fmt.Println("err=", err)
	}
	return []string{}
}

func FileComplete(prefix string, args []string) []string {
	return fsComplete(prefix, args, false)
}

func DirComplete(prefix string, args []string) []string {
	return fsComplete(prefix, args, true)
}

// func WsComplete(prefix string, args []string) []string {
// 	wslist := viper.GetStringSlice("workspaces")
// 	retv := []string{}
// 	for _, ws := range wslist {
// 		if strings.HasPrefix(ws, prefix) {
// 			retv = append(retv, strings.Replace(ws, prefix, "", 1))
// 		}
// 	}
// 	return retv
// }

func CredComplete(prefix string, args []string) []string {
	var creds []DbCredential
	err := Db.Prefix("Name", prefix, &creds)
	retv := []string{}
	if err != nil {
		return retv
	}
	for _, cc := range creds {
		retv = append(retv, cc.Name)
	}
	return retv
}

func MailComplete(prefix string, args []string) []string {
	creds := ListCredentialByPrefix(prefix)
	retv := []string{}
	if creds != nil {
		for _, cc := range *creds {
			if cc.Type == "mail" {
				retv = append(retv, cc.Name)
			}
		}
	}
	return retv
}

func TodoComplete(prefix string, args []string) []string {
	fmt.Printf("%v", args)
	todos := ListTodoByPrefix(prefix)
	retv := []string{}
	if todos != nil {
		for _, cc := range *todos {
			retv = append(retv, cc.Name)
		}
	}
	return retv
}

func WsComplete(prefix string, args []string) []string {
	ws := ListWorkspaceByPrefix(prefix)
	retv := []string{}
	if ws != nil {
		for _, cc := range *ws {
			retv = append(retv, cc.Name)
		}
	}
	return retv
}
