package util

import (
	"sync"

	log "github.com/sirupsen/logrus"
	messagebus "github.com/vardius/message-bus"
)

var SystemBus = messagebus.New(1000)

type SystemModule struct {
	Name   string
	Wg     sync.WaitGroup
	Active bool
	Run    func()
	Events func(event interface{})
}

func NewSystemModule(
	moduleName string,
	runFunc func(),
	eventFunc func(event interface{})) *SystemModule {
	retv := &SystemModule{
		Name:   moduleName,
		Active: false,
		Run:    runFunc,
		Events: eventFunc,
	}
	err := SystemBus.Subscribe("shutdown", func(cause string) {
		retv.Stop()
	})
	if err != nil {
		log.Error(err)
	}
	if eventFunc != nil {
		err = SystemBus.Subscribe(moduleName, eventFunc)
		if err != nil {
			log.Error(err)
		}
	}
	return retv
}

func (sm *SystemModule) Start() {
	if sm.Active {
		return
	}
	sm.Wg.Add(1)
	go func() {
		sm.Active = true
		if sm.Run != nil {
			sm.Run()
		}
		sm.Wg.Wait()
		sm.Active = false
	}()
}

// Stop - stop module run
func (sm *SystemModule) Stop() {
	log.Infof("Stopping system module: %v", sm.Name)
	if sm.Active {
		sm.Wg.Done()
	}
	sm.Active = false
}
