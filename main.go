package main

import (
	"deck/cmd"
	"deck/util"
	"fmt"
	"os"
	"time"

	"github.com/desertbit/grumble"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	configFilePath string
)

func init() {
	cdir, _ := os.UserHomeDir()
	cdir = cdir + string(os.PathSeparator) + ".deck"

	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)
	configFilePath = cdir + string(os.PathSeparator) + "config.yml"
	viper.SetConfigName("config")
	viper.AddConfigPath(cdir)
	viper.SetConfigType("yaml")
	logname := fmt.Sprintf("%v/logs/%%Y-%%m-%%d.log", cdir)
	rl, _ := rotatelogs.New(logname, rotatelogs.WithRotationTime(time.Hour*24))
	log.SetOutput(rl)
	util.DbPath = fmt.Sprintf("%v/deck.db", cdir)
	err := util.InitDb()
	if err != nil {
		panic(err)
	}
}

func shutdown() error {
	fmt.Println("Closing deck...")
	//util.SystemBus.Publish("shutdown", "system")
	err := util.Db.Close()
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

func main() {
	err := viper.ReadInConfig()
	if err != nil {
		util.SetDefaultConfig()
		err = viper.WriteConfigAs(configFilePath)
		if err != nil {
			fmt.Printf("%v", err)
		}
	}
	cmd.App.OnClose(shutdown)
	cmd.App.OnShell(func(a *grumble.App) error {
		cmd.AddStandardAliases(a)
		return nil
	})
	err = cmd.App.Run()
	if err != nil {
		fmt.Println(err)
	}
}
