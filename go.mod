module deck

go 1.12

require (
	github.com/JamesClonk/go-todotxt v0.0.0-20180101102949-97a991fa2677
	github.com/JohannesKaufmann/html-to-markdown v0.0.0-20180527153230-2c0dde123240 // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/awnumar/memguard v0.20.0
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/desertbit/grumble v1.0.3
	github.com/emersion/go-imap v1.0.0
	github.com/emersion/go-message v0.10.4-0.20190609165112-592ace5bc1ca
	github.com/go-openapi/strfmt v0.19.3 // indirect
	github.com/gomarkdown/markdown v0.0.0-20190912180731-281270bc6d83
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/lestrrat-go/file-rotatelogs v2.2.0+incompatible
	github.com/lestrrat-go/strftime v0.0.0-20190725011945-5c849dd2c51d // indirect
	github.com/logrusorgru/aurora v0.0.0-20190803045625-94edacc10f9b
	github.com/lunny/html2md v0.0.0-20181018071239-7d234de44546
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mattn/godown v0.0.0-20180312012330-2e9e17e0ea51 // indirect
	github.com/minio/sio v0.2.0
	github.com/sirupsen/logrus v1.2.0
	github.com/skratchdot/open-golang v0.0.0-20190402232053-79abb63cd66e
	github.com/spf13/viper v1.4.0
	github.com/vardius/message-bus v1.1.4
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/net v0.0.0-20191011234655-491137f69257 // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
	golang.org/x/tools v0.0.0-20191012152004-8de300cfc20a // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	gopkg.in/AlecAivazis/survey.v1 v1.8.5
)
